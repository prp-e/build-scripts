# Build Scripts for Sana Desktop 

## Preriquesties 

* A linux based environment (preferably Debian or Ubuntu. WSL or the system running on a VPS is also fine.) 
_ATTENTION_ : If you run this script inside a docker image, make sure the image has its own kernel installed. 
* This pieces of software : 

```xorriso grub2 squashfs-tools debootstrap mtools grub-pc-bin grub-efi-amd64-bin``` 

_NOTE_: If you run Ubuntu, most of these are installed. But for a better experience, just install all of them. If you have some of them installed, `apt` will let you know. 

* A terminal emulator. 
* Plenty of time :-) 

## About the script 

This script is influenced by William Haley's tutorial ([Link](https://willhaley.com/blog/custom-debian-live-environment/)) on how to make a custom debian live disc. 

## Options 

* `make_folders` : Creates necessary directories. 
* `bootstrap` : Creates a bootstrapped debian system. 
* `kernel_install` : _in progress_. 
* `install_basics` : installs some basic tools needed for a basic live disc. 
* `install_environment`: _in progress_. 
* `boot_loader` : Creates all boot prereqisties. 
* `filesystem` : Creates SquashFS. 
* `iso` : Creates a bootable ISO image from the base system. 
* `clean` : Cleans the directory. 

_ATTENTION_ : Do not run the script with `sudo` unless it says that sudo is needed. 

## Guide to make a system using this script 

First, you need to run script like this : 

```
./build_sana.sh make_folders 
``` 

This makes necessary folders for a base system. After this, you need to bootstrap a Debian base. For this, just run this script with root permissions: 

```
sudo ./build_sana.sh bootstrap 
``` 

`kernel_install` doesn't work for now. you have to install it manually using `chroot`. But `install_basics` works. Never forget to run that with root permissions. 

After these, make the filesystem with root permissions: 

```
sudo ./build_sana.sh filesystem
``` 

Then, prepare a proper boot loader: 

```
./build_sana.sh boot_loader
``` 

after all these, you freely can make a bootable ISO : 

```
./build_sana.sh iso 
``` 

After testing the live disc, you can clean the directory: 

```
./build_sana.sh clean
``` 