#!/bin/bash 

rootfs="chroot"
live="liveboot" 
inner_live="live" 
inner_image="image" 
boot_options="scratch"

case $1 in 
    make_folders)
        mkdir -v $rootfs
        mkdir -pv $live/{$boot_options,$inner_image/$inner_live}
    ;;

    bootstrap)
        if [ $(whoami) == 'root' ] 
        then 
            debootstrap testing $rootfs http://deb.debian.org/debian 
        else
            echo "Please run this part with sudo!"
        fi
    ;;

    kernel_install)
       echo "This part needs some improvements."
    ;;

    install_basics)
        if [ $(whoami) == 'root' ]
        then 
            chroot $rootfs apt update && apt install network-manager net-tools wireless-tools wpagui curl openssh-client
        else 
            echo "Please run this part with sudo!"
        fi
    ;;

    install_environment)
        echo "This option will be developed soon"
    ;; 

    boot_loader)
        echo "insmod all_video"  > $live/$boot_options/grub.cfg 
        echo "search --set=root --file /DEBIAN_CUSTOM" >> $live/$boot_options/grub.cfg
        echo "" >> $live/$boot_options/grub.cfg 
        echo "set default=0" >> $live/$boot_options/grub.cfg 
        echo "set timeout=10" >> $live/$boot_options/grub.cfg 
        echo "" >> $live/$boot_options/grub.cfg 
        echo "menuentry 'Sana Desktop' {" >> $live/$boot_options/grub.cfg 
        echo " linux /vmlinuz boot=live quiet nomodeset" >> $live/$boot_options/grub.cfg
        echo " initrd /initrd" >> $live/$boot_options/grub.cfg
        echo "}" >> $live/$boot_options/grub.cfg 

        echo "grub.cfg successfully created" 

        cp -iv $rootfs/boot/vmlinuz-* $live/$inner_image/vmlinuz 
        cp -iv $rootfs/boot/initrd.img-* $live/$inner_image/initrd 

        echo "Base boot system successfully created."

        grub-mkstandalone --format=i386-pc --output=$live/$boot_options/core.img --install-modules="linux normal iso9660 biosdisk memdisk search tar ls" --modules="linux normal iso9660 biosdisk search" --locales="" --fonts="" "boot/grub/grub.cfg=$live/$boot_options/grub.cfg" 
        cat /usr/lib/grub/i386-pc/cdboot.img $live/$boot_options/core.img > $live/$boot_options/bios.img 

        echo "Boot loader and process successfully created!"
        ;; 

    filesystem)
        touch $live/$inner_image/DEBIAN_CUSTOM

        if [ $(whoami) == 'root' ]
        then
            mksquashfs $rootfs $live/$inner_image/$inner_live/filesystem.squashfs 
            echo "SquashFS created successfully!"
        else
            echo "Please run this part with sudo"
        fi 
    ;;

    iso) 
        xorriso -as mkisofs -iso-level 3 -full-iso9660-filenames -volid "DEBIAN_CUSTOM" --grub2-boot-info --grub2-mbr /usr/lib/grub/i386-pc/boot_hybrid.img -eltorito-boot boot/grub/bios.img -no-emul-boot -boot-load-size 4 -boot-info-table --eltorito-catalog boot/grub/boot.cat -output "Sana_Live.iso" -graft-points "$live/$inner_image" /boot/grub/bios.img=$live/$boot_options/bios.img 
    ;; 

    clean)
        if [ $(whoami) == 'root' ]
        then 
         rm -rvf chroot liveboot 
        else 
         echo "Run this with sudo :-)"
        fi 

        rm -rvf *.iso 

        echo "Directory cleaned successfully!"
    ;;

esac 